// $(document).ready(function () {

document.getElementById('header').innerHTML = `
<nav class="navbar navbar-expand-lg navigation fixed-top">
		<div class="container-fluid">
			<a class="navbar-brand" href="index.html">
				<img src="http://testyantra.com/sites/default/files/tylog1.png" alt="" class="logotag">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
				aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation">
				<span class="ti-view-list"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbar">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="" id="dropdown01" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">COMPANY</a>
						<div class="dropdown-menu megamenu" aria-labelledby="dropdown01">
							<div class="row">
								<div class="col-sm-6 col-lg-4">
									<!-- <h5>Links</h5> -->
									<a class="dropdown-item" href="./about.html">ABOUT US</a>
									<a class="dropdown-item" href="/404.html">OUR TEAM</a>
									<a class="dropdown-item" href="/404.html">OUR ACADEMY</a>
									<a class="dropdown-item" href="/404.html">KNOWLEDGE CENTRE</a>
									<a class="dropdown-item" href="/404.html">OTHER WEBSITES/ BRANDS</a>
									<a class="dropdown-item" href="/404.html">DOWNLOADS</a>
								</div>

							</div>
						</div>
					</li>
					<li class="nav-item dropdown megamenu-li">
						<a class="nav-link dropdown-toggle" href="" id="dropdown02" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">Services</a>
						<div class="dropdown-menu" id="service-dropdown" aria-labelledby="dropdown02"
							style="width: 80%;margin-left: 10%;margin-top: 0 !important;" >
							<div class="row" >
								<div class="col-sm-6 col-lg-4" >
									<h5 class="text-white center">DEVELOPMENT</h5>
									<a class="dropdown-item" href="development.html">Software Development</a>
									<a class="dropdown-item" href="/404.html">Creative Website Design </a>
									<a class="dropdown-item" href="/404.html">Analytics and insights</a>
									<a class="dropdown-item" href="/404.html">It Consulting</a>
									<a class="dropdown-item" href="/404.html">Machine Learning and <br>data science</a>
									<a class="dropdown-item" href="/404.html">API development</a>
									<a class="dropdown-item" href="/404.html">devops</a>
								</div>
								<div class="col-sm-6 col-lg-4">
									<h5 class="text-white center">CORE QA SERVICES</h5>
									<a class="dropdown-item" href="/404.html">FIST (FUNCTIONAL INTEGRATION SYSTEM)</a>
									<a class="dropdown-item" href="./regression-testing.html">REGRESSION</a>
									<a class="dropdown-item" href="/404.html">COMPATIBILITY</a>
									<a class="dropdown-item" href="/404.html">USABILITY</a>
									<a class="dropdown-item" href="/404.html">EXPLORATORY</a>
									<a class="dropdown-item" href="/404.html">UAT</a>
								</div>
								<div class="col-sm-6 col-lg-4">
									<h5 class="text-white center">SPECIALIZED QA SERVICES</h5>
									<a class="dropdown-item" href="./services-test_automation.html">AUTOMATION</a>
									<a class="dropdown-item" href="/404.html">PERFORMANCE</a>
									<a class="dropdown-item" href="/404.html">SECURITY</a>
									<a class="dropdown-item" href="/404.html">DATAWAREHOUSE</a>
									<a class="dropdown-item" href="/404.html">TEST DATA MANAGEMENT</a>
								</div>
								<div class="col-sm-6 col-lg-4">
									<h5 class="text-white center">STRATEGIC QA SERVICES</h5>
									<a class="dropdown-item" href="/404.html">TCOE/ TCC</a>
									<a class="dropdown-item" href="/404.html">SCALED AGILE/ SAFE</a>
									<a class="dropdown-item" href="/404.html">DEVOPS</a>
									<a class="dropdown-item" href="/404.html">QA PARTNER - FULL TEST LIFE CYCLE</a>
									<a class="dropdown-item" href="./qa-consulting-and-advisory-services.html">QA CONSULTING AND ADVISORY</a>
									<a class="dropdown-item" href="/404.html">STRATEGIC STAFFING</a>
								</div>
								<div class="col-sm-6 col-lg-4">
									<h5 class="text-white center">JUST IN TIME QA SERVICES</h5>
									<a class="dropdown-item" href="/404.html">JUST-IN-TIME STAFFING</a>
									<a class="dropdown-item" href="/404.html">HIRE TRAIN DEPLOY</a>
									<a class="dropdown-item" href="/404.html">BUILD OPERATE TRANSFER</a>
									<a class="dropdown-item" href="/test-factory.html">TEST FACTORY </a>
								</div>
								<!-- <div class="col-sm-6 col-lg-4">
									<h5 class="center text-white">TESTING</h5>
									<a class="dropdown-item" href="./services-qa.html">Managed QA Services</a>
									<a class="dropdown-item" href="./services-test_automation.html">Test Automation
										Services</a>
									<a class="dropdown-item" href="crowd.html">Crowd testing</a>
									<a class="dropdown-item" href="/404.html">professional services</a>
									<a class="dropdown-item" href="mobile.html">Mobile Application</a>
									<a class="dropdown-item" href="iottesting.html">Iot Testing</a>
									<a class="dropdown-item" href="./services-insurance-testing.html">Insurance Testing
										Services</a>
									<a class="dropdown-item" href="./regression-testing.html">Regression Testing</a>
									<a class="dropdown-item" href="test-factory.html">Test Factory</a>
								</div>

								<div class="col-sm-6 col-lg-4">
									<h5 class="center text-white">TRAINING</h5>
									<a class="dropdown-item" href="training.html">Classroom training</a>
									<a class="dropdown-item" href="/404.html">online training</a>
									<a class="dropdown-item" href="/404.html">corporate training</a>
									<a class="dropdown-item" href="/404.html">exponential learning <br>factory(elf)</a>
								</div> -->
							</div>
						</div>
					</li>
					<li class="nav-item dropdown megamenu-li">
						<a class="nav-link dropdown-toggle" href="" id="dropdown03" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">SOLUTIONS</a>
						<div class="dropdown-menu " id="service-dropdown" aria-labelledby="dropdown03"
							style="width: 80%;margin-left: 10%;margin-top: 0 !important;">
							<div class="row">
								<div class="col-sm-6 col-lg-4">
									<h5 class="text-white center">CROWD TESTING </h5>
									<a class="dropdown-item" href="/crowd.html">CROWDBETATESTERS</a>
								</div>
								<div class="col-sm-6 col-lg-4">
									<h5 class="text-white center">NLP BASED AUTOMATION </h5>
									<a class="dropdown-item" href="/404.html">TEST OPTIMISE</a>
								</div>
								<div class="col-sm-6 col-lg-4">
									<h5 class="text-white center">SKILL LIBRARY</h5>
									<a class="dropdown-item" href="/404.html">SKILLRARY ASSESSMENTS</a>
									<a class="dropdown-item" href="/404.html">SKILLRARY LIVE</a>
									<a class="dropdown-item" href="/404.html">SKILLRARY CODETEST</a>									
								</div>
							</div>
						</div>
					</li>		
					<li class="nav-item dropdown ">
						<a class="nav-link dropdown-toggle" href="" id="dropdown04" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">DIGITAL ASSURANCE</a>
						<div class="dropdown-menu megamenu" aria-labelledby="dropdown04">
							<div class="row">
								<div class="col-sm-6 col-lg-4">
									<!-- <h5>Links</h5> -->
									<a class="dropdown-item" href="/mobile.html">MOBILE</a>
									<a class="dropdown-item" href="/404.html">CLOUD</a>
									<a class="dropdown-item" href="/404.html">AI/ ML</a>
									<a class="dropdown-item" href="iot.html">IOT</a>
									<a class="dropdown-item" href="/404.html">BIG DATA</a>
									<a class="dropdown-item" href="/404.html">DATA SCIENCE</a>
								</div>
							</div>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="" id="dropdown05" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">INDUSTRIES</a>
						<div class="dropdown-menu megamenu" aria-labelledby="dropdown05">
							<div class="row">
								<div class="col-sm-6 col-lg-4">
									<!-- <h5>Links</h5> -->															
									<a class="dropdown-item" href="/404.html">BFSI</a>
									<a class="dropdown-item" href="/404.html">TELECOM</a>
									<a class="dropdown-item" href="./industries-retail-domain.html">RETAIL AND ECOMMERCE</a>
									<a class="dropdown-item" href="/404.html">HEALTHCARE</a>
									<a class="dropdown-item" href="./industries-media&entertainment.html">MEDIA AND ENTERTAINMENT</a>
									<a class="dropdown-item" href="./industries-education.html">EDUCATION</a>
									<a class="dropdown-item" href="/404.html">ENERGY AND UTILITIES</a>
									<a class="dropdown-item" href="/404.html">TECHNOLOGY</a>
								</div>
							</div>
						</div>
					</li>
					<!-- <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="" id="dropdown02" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">case studies</a>
						<div class="dropdown-menu megamenu" aria-labelledby="dropdown02">
							<div class="row">
								<div class="col-sm-6">
									<h5>Links</h5>
									<a class="dropdown-item" href="casestudy.html">Case study</a>
								</div>
							</div>
						</div>
					</li> -->
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="" id="dropdown06" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">CONTACT US</a>
						<div class="dropdown-menu megamenu" aria-labelledby="dropdown06">
							<div class="row">
								<div class="col-sm-6">
									<a class="dropdown-item" href="/global-appearance.html">GEOGRAPHIES</a>
									<a class="dropdown-item" href="/404.html">OTHER WEBSITES/ BRANDS</a>
								</div>
							</div>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="" id="dropdown07" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">CAREERS</a>
						<div class="dropdown-menu megamenu" aria-labelledby="dropdown07">
							<div class="row">
								<div class="col-sm-6">
									<a class="dropdown-item" href="/404.html">SIGN UP/ LOGIN</a>
								</div>
							</div>
						</div>
					</li>
					<!-- <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="" id="dropdown02" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false">company</a>
						<div class="dropdown-menu megamenu" aria-labelledby="dropdown02">
							<div class="row">
								<div class="col-sm-6">
									<h5>Links</h5>
									<a class="dropdown-item" href="about.html">About Us</a>
									<a class="dropdown-item" href="leader.html">Leader Ship</a>
									<a class="dropdown-item" href="career.html">Career</a>
									<a class="dropdown-item" href="global-appearance.html">Global Appearance</a>
								</div>
							</div>
						</div>
					</li> -->
				</ul>
			</div>
		</div>
	</nav>
`;

;(function ($) {
	'use strict';

  $(window).scroll(function () {
    if ($('.navigation').offset().top > 100) {
      $('.navigation').addClass('fixed-nav');
    } else {
      $('.navigation').removeClass('fixed-nav');
    }
  });
  

 $('.portfolio-gallery').each(function () {
        $(this).find('.popup-gallery').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });
    });


	$('#contact-form').validate({
		rules: {
			user_name: {
				required: true,
				minlength: 4
			},
			user_email: {
				required: true,
				email: true
			},
			// user_subject: {
			// 	required: false
			// },
			user_message: {
				required: true
			}
		},
		messages: {
			user_name: {
				required: 'Come on, you have a name don\'t you?',
				minlength: 'Your name must consist of at least 2 characters'
			},
			user_email: {
				required: 'Please put your email address'
			},
			user_message: {
				required: 'Put some messages here?',
				minlength: 'Your name must consist of at least 2 characters'
			}

		},
		submitHandler: function (form) {
			$(form).ajaxSubmit({
				type: 'POST',
				data: $(form).serialize(),
				url: 'sendmail.php',
				success: function () {
					$('#contact-form #success').fadeIn();
				},
				error: function () {

					$('#contact-form #error').fadeIn();
				}
			});
		}
	});



	

	$('.partners-slider').slick({
		slidesToShow: 3,
		infinite: true,
		arrows: false,
		autoplay: true,
		autoplaySpeed: 5000,
		dots:true
	});




	// Init Magnific Popup
	$('.portfolio-popup').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled: true
		},
		mainClass: 'mfp-with-zoom',
		navigateByImgClick: true,
		arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
		tPrev: 'Previous (Left arrow key)',
		tNext: 'Next (Right arrow key)',
		tCounter: '<span class="mfp-counter">%curr% of %total%</span>',
		zoom: {
			enabled: true,
			duration: 300,
			easing: 'ease-in-out',
			opener: function (openerElement) {
				return openerElement.is('img') ? openerElement : openerElement.find('img');
			}
		}
	});




	var map;

	function initialize() {
		var mapOptions = {
			zoom: 13,
			center: new google.maps.LatLng(50.97797382271958, -114.107718560791)
			// styles: style_array_here
		};
		map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	}

	var google_map_canvas = $('#map-canvas');

	if (google_map_canvas.length) {
		google.maps.event.addDomListener(window, 'load', initialize);
	}




})(jQuery);



// stats counter

(function ($) {
	$.fn.countTo = function (options) {
		options = options || {};
		
		return $(this).each(function () {
			// set options for current element
			var settings = $.extend({}, $.fn.countTo.defaults, {
				from:            $(this).data('from'),
				to:              $(this).data('to'),
				speed:           $(this).data('speed'),
				refreshInterval: $(this).data('refresh-interval'),
				decimals:        $(this).data('decimals')
			}, options);
			
			// how many times to update the value, and how much to increment the value on each update
			var loops = Math.ceil(settings.speed / settings.refreshInterval),
				increment = (settings.to - settings.from) / loops;
			
			// references & variables that will change with each update
			var self = this,
				$self = $(this),
				loopCount = 0,
				value = settings.from,
				data = $self.data('countTo') || {};
			
			$self.data('countTo', data);
			
			// if an existing interval can be found, clear it first
			if (data.interval) {
				clearInterval(data.interval);
			}
			data.interval = setInterval(updateTimer, settings.refreshInterval);
			
			// initialize the element with the starting value
			render(value);
			
			function updateTimer() {
				value += increment;
				loopCount++;
				
				render(value);
				
				if (typeof(settings.onUpdate) == 'function') {
					settings.onUpdate.call(self, value);
				}
				
				if (loopCount >= loops) {
					// remove the interval
					$self.removeData('countTo');
					clearInterval(data.interval);
					value = settings.to;
					
					if (typeof(settings.onComplete) == 'function') {
						settings.onComplete.call(self, value);
					}
				}
			}
			
			function render(value) {
				var formattedValue = settings.formatter.call(self, value, settings);
				$self.html(formattedValue);
			}
		});
	};
	
	$.fn.countTo.defaults = {
		from: 0,               // the number the element should start at
		to: 0,                 // the number the element should end at
		speed: 1000,           // how long it should take to count between the target numbers
		refreshInterval: 100,  // how often the element should be updated
		decimals: 0,           // the number of decimal places to show
		formatter: formatter,  // handler for formatting the value before rendering
		onUpdate: null,        // callback method for every time the element is updated
		onComplete: null       // callback method for when the element finishes updating
	};
	
	function formatter(value, settings) {
		return value.toFixed(settings.decimals);
	}
}(jQuery));

jQuery(function ($) {
  // custom formatting example
  $('.count-number').data('countToOptions', {
	formatter: function (value, options) {
	  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
	}
  });
  
  // start all the timers
  $('.timer').each(count);  
  
  function count(options) {
	var $this = $(this);
	options = $.extend({}, options || {}, $this.data('countToOptions') || {});
	$this.countTo(options);
  }
});

// end //