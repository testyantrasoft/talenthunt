import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.css']
})
export class SurveyListComponent implements OnInit {
  surveyNames = [];
  constructor(private router:Router) { }

  ngOnInit() {
    console.log(localStorage.getItem('adminPreiew'));
    this.surveyNames = JSON.parse(localStorage.getItem('adminPreiew'));
    console.log(this.surveyNames);
  }

  navigateToAddQuestions(){
  this.router.navigateByUrl('/questions');
  }

}
